﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Debug_Tracker
{

    public partial class Form1 : Form
    {        
        static DbAccess  dbAcc = new DbAccess();
        List<Bug> bugList;


        public Form1()
        {
            InitializeComponent();            
            //checkConnection();

            bugList = dbAcc.getBugs();

            if (bugList == null)
            {

            } 
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
           
        }

        private void checkConnection()
        {
            dbAcc.getBugs();
        }
        
        // Add Bug button
        private void button1_Click(object sender, EventArgs e)
        {            
            AddBug addBugForm   =   new AddBug();
            addBugForm.Show();
        }

        // View Bugs button 
        private void button2_MouseClick(object sender, MouseEventArgs e)
        {
            Form2 viewBugForm   =   new Form2();
            viewBugForm.Show();
        }
    }   
}
