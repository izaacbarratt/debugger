﻿CREATE TABLE [dbo].[Table]
(
	[id] INT NOT NULL PRIMARY KEY, 
    [desc] NVARCHAR(50) NULL, 
    [status] NCHAR(10) NOT NULL, 
    [date_added] DATETIME NOT NULL, 
    [last_update] DATETIME NOT NULL, 
    [line_number] INT NOT NULL
)
