﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Debug_Tracker
{
    public partial class Form2 : Form
    {
        ListView bugLV;

        public Form2()
        {
            InitializeComponent();
            bugLV = this.listView1;
            bugLV.GridLines = true;
            bugLV.View = View.Details;
            bugLV.Size = this.Size;

            DbAccess dbAcc = new DbAccess();
            List<Bug> listBug = dbAcc.getBugs();

            ListViewItem lvi1 = new ListViewItem("item1");
            lvi1.SubItems.Add("1y");
            lvi1.SubItems.Add("2y");
            lvi1.SubItems.Add("3y");
            lvi1.SubItems.Add("4y");
            ListViewItem lvi2 = new ListViewItem("item2");
            lvi2.SubItems.Add("1z");
            lvi2.SubItems.Add("2z");
            ListViewItem lvi3 = new ListViewItem("item3");
            lvi3.SubItems.Add("1x");
            lvi3.SubItems.Add("3x");
            lvi3.SubItems.Add("4x");

            bugLV.Columns.Add("ID", -2, HorizontalAlignment.Left);
            bugLV.Columns.Add("Line #", -2, HorizontalAlignment.Left);
            bugLV.Columns.Add("Description", -2, HorizontalAlignment.Left);
            bugLV.Columns.Add("Status", -2, HorizontalAlignment.Left);            
            bugLV.Columns.Add("Last Update", -2, HorizontalAlignment.Left);            
            bugLV.Columns.Add("Date Added", -2, HorizontalAlignment.Left);

            bugLV.Items.AddRange(new ListViewItem[] { lvi1, lvi2, lvi3});

            /*
            for (int i = 0; i < listBug.Count; i++)
            {
                listView1.Items.Add( listBug.ElementAt(i).getId().ToString() );
            }*/
        }


        // If you select on the listview 
        private void listView1_Click(object sender, EventArgs e)
        {

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        // If you chnage size of form, changes the listview appropriately 
        private void Form2_SizeChanged(object sender, EventArgs e)
        {
            bugLV.Size = this.Size;
        }
    }
}
