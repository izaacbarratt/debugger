﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Debug_Tracker
{
    class Bug
    {
        int     id;        
        string  desc;
        int     lineNumber;
        string  status;

        string  date_added;
        string  last_update;        
        
        int     project_id;
        int     class_id;



        // Constructor func - Used for making your own new bug
        public Bug(string desc, int lineNumber, string status)
        {
            this.desc = desc;
            this.lineNumber = lineNumber;
            this.status = status;
        }        
        // Constructor func - Used for displaying from database
        public Bug(int id, string desc, int lineNumber, string status, string date_added, string last_update, int proj_id, int class_id)
        {
            Console.WriteLine("tried do that thing on the acc func but nah b");
            this.id         =   id;
            this.desc       =   desc;
            this.lineNumber =   lineNumber;
            this.status     =   status;
            this.date_added =   date_added;
            this.last_update=   last_update;
            this.project_id =   proj_id;
            this.class_id   =   class_id;
        }
        // Constructor func - Used for making your own new bug
        public Bug(int id, string desc, int lineNumber, string status)
        {
            this.id     =   id;
            this.desc   =   desc;
            this.lineNumber = lineNumber;
            this.status     = status;
        }



        // Set Location of class & project
        public void setLocation(int projid, int classid)
        {
            project_id = projid;
            class_id = classid;
        }

        //Change status of bug
        public void setStatus(string nwstr)
        {
            status = nwstr;
        }

        //GET FUNCTIONS FOR VARIABLES --
        public int getId()
        {
            return id;
        }      



        // Prints data in output
        public void printData()
        {
            string  outputS =   "-- BUG : ";

            outputS += id.ToString();
            outputS += ", ";
            outputS += desc;
            outputS += ", ";
            outputS += lineNumber.ToString();
            outputS += ", ";
            outputS += date_added;
            outputS += ", ";
            outputS += status;
            Console.WriteLine(outputS);
        }
    }
}
